// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyAIz7bW_OrphMX792CfYrCP-v6PnWiMROg',
    authDomain: 'team-on-fire.firebaseapp.com',
    databaseURL: 'https://team-on-fire.firebaseio.com',
    projectId: 'team-on-fire',
    storageBucket: '',
    messagingSenderId: '649008660962'
  },
  API_MANAGER_URL: 'https://api-manager.kube.vged.es/am/token',
  API_MANAGER_BODY: 'grant_type=client_credentials&client_id=fyjKU7bSWbSlvjN_SewTy9SDNXca&client_secret=masW986bV1UB9xwSTGfSYXrn9aAa',
  API_MANAGER_HEADER: 'application/x-www-form-urlencoded',
  KEYCLOAK_INIT_CONFIG: {
    config: {
        realm: 'vaesa',
        url: 'http://localhost:4000/auth',
        clientId: 'vaesa'
    },
    initOptions: {
        onLoad: 'login-required',
        checkLoginIframe: false
    },
    enableBearerInterceptor: false,
    bearerExcludedUrls: [
        '/assets',
        '/sockjs-node',
        '/clients/public',
        '/gestionobjetivos-backend'
    ]
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
