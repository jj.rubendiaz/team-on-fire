import { Team } from '../models/Team';
import { TeamsActionsUnion, TeamsActionTypes } from '../actions/teams.actions';

export const initialState = [];

export function teamsReducer(
    state = initialState,
    action: TeamsActionsUnion): Team[] {
    const teams = [...state];
    switch (action.type) {
        case TeamsActionTypes.Add:
            teams.push(action.payload);
            return teams;

        case TeamsActionTypes.Delete:
            teams.splice(action.payload, 1);
            return teams;

        case TeamsActionTypes.Reset:
            return action.payload;

        default:
            return state;
    }
}
