import { Action } from '@ngrx/store';
import { MessageActionTypes, ActionsUnion } from '../actions/message.actions';

export const initialState = 'Seleccione un equipo para ver su mensaje.'

export function messageReducer(
    state = initialState,
    action: ActionsUnion): string {
    switch (action.type) {
        case MessageActionTypes.Add:
            return action.payload;

        case MessageActionTypes.Delete:
            return '';

        case MessageActionTypes.Reset:
            return 'Seleccione un equipo para ver su mensaje.';

        default:
            return state;
    }
}
