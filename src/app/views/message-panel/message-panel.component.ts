import { Component, OnInit } from '@angular/core';
import { MessageService } from 'src/app/providers/message.service';
import { Observable } from 'rxjs';
import { TeamService } from 'src/app/providers/team.service';
import { Team } from 'src/app/models/Team';
import { map } from 'rxjs/operators';
import { AngularFirestore } from '@angular/fire/firestore';
import { Store, select } from '@ngrx/store';
import { Add, Delete, Reset } from 'src/app/actions/message.actions';
@Component({
  selector: 'app-message-panel',
  templateUrl: './message-panel.component.html',
  styleUrls: ['./message-panel.component.scss']
})
export class MessagePanelComponent implements OnInit {
  activeTeam: Observable<{}>;
  teams: Observable<any[]>;
  message$: Observable<string>;

  constructor(
    private db: AngularFirestore,
    private messageService: MessageService,
    private teamService: TeamService,
    private store: Store<{message: string, teams: Team[]}>) {
      this.message$ = this.store.pipe(select('message'));
  }

  ngOnInit() {
    this.teams = this.teamService.teams.pipe(
      map((teams: Team[]) => {
        return teams.map( team => {
          return {
            id: team.id,
            value: team.message,
            display: team.name
          };
        });
      })
    );
  }

  activateTeam(team) {
    this.db.doc(`teams/${team.id}`).valueChanges().subscribe((data: any) => {
      this.activeTeam = data;
      this.store.dispatch(new Add(data.message));
    });
  }
}
