import { Component, OnInit } from '@angular/core';
import { TeamService } from 'src/app/providers/team.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Team } from 'src/app/models/Team';
import { Store } from '@ngrx/store';
import { Option } from 'src/app/models/Option';

@Component({
  selector: 'app-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.scss']
})
export class EditorComponent implements OnInit{
  teams$: Observable<Option[]>;
  activeTeam: any;
  active = false;

  constructor(private state: Store<{message: string, teams: Team[]}>, private teamService: TeamService) {
    this.teams$ = this.state.select('teams').pipe(
      map((teams: Team[]) => {
        return teams.map((team: Team) => {
          return {
            id: team.id,
            value: team.message,
            display: team.name
          };
        });
      })
    );
  }

  ngOnInit() {
    this.teamService.getTeams();
  }

  updateMessage(team) {
    this.teamService.updateTeam(team.id, {name: team.display, message: team.value});
    this.active = false;
  }

  setActiveTeam(team) {
    this.activeTeam = team;
    console.log(team);
  }

}
