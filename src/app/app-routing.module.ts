import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MessagePanelComponent } from './views/message-panel/message-panel.component';
import { EditorComponent } from './views/editor/editor.component';
import { AuthGuard } from './auth/auth-guard.service';

const routes: Routes = [{
  path: 'message', component: MessagePanelComponent
}, {
  path: 'settings', component: EditorComponent, canActivate: [AuthGuard]
}, {
  path: '**', component: EditorComponent, canActivate: [AuthGuard]
}];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
