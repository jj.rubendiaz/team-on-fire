export interface Team {
    name: string;
    message: string;
    id: string;
}
