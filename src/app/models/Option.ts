export interface Option {
    id: string;
    value: string;
    display: string;
}
