import { Injectable } from '@angular/core';
import { KeycloakService } from 'keycloak-angular';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private keycloak: KeycloakService) {}

  public isLogin(): Promise<boolean> {
    return this.keycloak.getToken().then((ssoJwt) => {
      localStorage.setItem('sso-jwt', ssoJwt);
      return ssoJwt ? true : false;
    });
  }
}
