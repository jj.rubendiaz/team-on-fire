import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { Router, CanActivate } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private auth: AuthService, public router: Router) { }

  canActivate(): Promise<boolean> | Observable<boolean> | boolean {
    return this.auth.isLogin();
  }
}
