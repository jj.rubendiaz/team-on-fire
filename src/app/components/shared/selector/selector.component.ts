import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

interface Option {
  id: string;
  value: string;
  display: string;
}

@Component({
  selector: 'app-selector',
  templateUrl: './selector.component.html',
  styleUrls: ['./selector.component.scss']
})
export class SelectorComponent implements OnInit {
  @Input() options: Option[];
  @Output() optionEmitter = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {
  }

  submit(value) {
    console.log('[ON CLICK EVENT EMITTER]', value);
    
    this.optionEmitter.emit(value);
  }
}
