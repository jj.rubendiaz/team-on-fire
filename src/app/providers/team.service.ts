import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { Team } from '../models/Team';
import { Reset } from '../actions/teams.actions';

@Injectable({
  providedIn: 'root'
})
export class TeamService {
  teams: Observable<{}[]>;
  constructor(
    private db: AngularFirestore,
    private state: Store<{message: string, teams: Team[]}>) {
      this.teams = this.db.collection('teams').snapshotChanges().pipe(
        map(data => {
          return data.map(item => {
            return {
              ...item.payload.doc.data(),
              id: item.payload.doc.id
            };
          });
        })
      );
  }

  getTeams() {
    this.db.collection('teams').snapshotChanges().subscribe(data => {
      const teams: any[] = data.map(item => {
        return {
          ...item.payload.doc.data(),
          id: item.payload.doc.id
        };
      });
      this.state.dispatch(new Reset(teams));
    });
  }

  updateTeam(id, team) {
    this.db.doc(`teams/${id}/`).set(team);
  }
}
