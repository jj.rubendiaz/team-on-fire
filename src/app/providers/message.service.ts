import { Injectable } from '@angular/core';
import { Subject, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MessageService {
  private activeMessageSource = new BehaviorSubject('Seleccione un equipo para ver su texto');

  constructor() { }

  get activeMessage$() {
    return this.activeMessageSource.asObservable();
  }

  set activeMessage(message: string) {
    this.activeMessageSource.next(message);
  }
}
