import { Component, OnInit } from '@angular/core';
import { Team } from './models/Team';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'team-on-fire';

  constructor(private state: Store<{message: string, teams: Team[]}>) {
    this.state.subscribe(data => {
      console.log('[APP STORE DATA]', data);
    });
  }

}
