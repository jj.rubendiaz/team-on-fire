import { Action } from '@ngrx/store';
import { Team } from '../models/Team';

export enum TeamsActionTypes {
    Add = '[Teams] Add',
    Delete = '[Teams] Delete',
    Reset = '[Teams] Reset'
}

export class Add implements Action {
    readonly type = TeamsActionTypes.Add;

    constructor(public payload: string) {}
}

export class Delete implements Action {
    readonly type = TeamsActionTypes.Delete;

    constructor(public payload: number) {}
}

export class Reset implements Action {
    readonly type = TeamsActionTypes.Reset;

    constructor(public payload: Team[]) {}
}

export type TeamsActionsUnion = Add | Delete | Reset;
