import { Action } from '@ngrx/store';

export enum MessageActionTypes {
    Add = '[Editor Component] Add',
    Delete = '[Editor Component] Delete',
    Reset = '[Editor Component] Reset'
}

export class Add implements Action {
    readonly type = MessageActionTypes.Add;

    constructor(public payload: string) {}
}

export class Delete implements Action {
    readonly type = MessageActionTypes.Delete;
}

export class Reset implements Action {
    readonly type = MessageActionTypes.Reset;
}

export type ActionsUnion = Add | Delete | Reset;
