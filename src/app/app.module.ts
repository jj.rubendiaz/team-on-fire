import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { environment } from 'src/environments/environment.prod';
import { MessagePanelComponent } from './views/message-panel/message-panel.component';
import { SelectorComponent } from './components/shared/selector/selector.component';
import { EditorComponent } from './views/editor/editor.component';
import { StoreModule } from '@ngrx/store';
import { messageReducer } from './reducers/message.reducer';
import { teamsReducer } from './reducers/teams.reducer';
import { initializer } from './utlis/app-init';
import { KeycloakService, KeycloakAngularModule } from 'keycloak-angular';

@NgModule({
  declarations: [
    AppComponent,
    MessagePanelComponent,
    SelectorComponent,
    EditorComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    KeycloakAngularModule,
    StoreModule.forRoot({message: messageReducer, teams: teamsReducer}),
    AngularFireModule.initializeApp(environment.firebase, 'team-on-fire'),
    AngularFirestoreModule,
    AngularFireStorageModule,
    FormsModule
  ],
  providers: [{
    provide: APP_INITIALIZER,
    useFactory: initializer,
    multi: true,
    deps: [KeycloakService]
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
